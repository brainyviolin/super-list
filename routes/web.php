<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->middleware('auth');

Route::get('/login', ['as' => 'login', 'uses' => 'PageController@login'])->middleware('guest');
Route::post('/login', ['as' => 'authenticate', 'uses' => 'PageController@authenticate'])->middleware('guest');
Route::get('/logout', 'PageController@logout');