<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['prefix' => '/tasks'], function () {

        Route::get('/', 'Api\TasksController@index');
        Route::post('/', 'Api\TasksController@store');
        Route::get('/{id}', 'Api\TasksController@show');
        Route::post('/{id}', 'Api\TasksController@update');
        Route::post('/{id}/order', 'Api\TasksController@updateOrder');
        Route::post('/{id}/mark-completed', 'Api\TasksController@markAsCompleted');
        Route::post('/{id}/delete', 'Api\TasksController@delete');

    });

    Route::group(['prefix' => '/users'], function(){

        Route::get('/', 'Api\UserController@index');
        Route::get('/me', 'Api\UserController@authenticatedUser');

    });

});