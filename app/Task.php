<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'text',
        'order',
        'due_date',
        'completed'
    ];

    protected $dates = ['deleted_at'];

    public function taskLogs()
    {
        return $this->hasMany('App\TaskLog');
    }


    /**
     * Increment the highest order value
     * @return integer
     */
    protected static function incrementedMaximumOrder()
    {
        return self::max('order') + 1;
    }

    /**
     * Insert new task with a new highest order
     * @param array $params
     * @return mixed
     */
    //@TODO : rename this method
    public static function createNew(array $params)
    {
        return self::create(array_add($params, 'order', self::incrementedMaximumOrder()));
    }

    /**
     * A readable named method for completing a task
     *
     * @return bool
     */
    public function markAsCompleted()
    {
        $this->completed = 1;
        return $this->save();
    }
}
