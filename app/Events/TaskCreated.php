<?php

namespace App\Events;

use App\Task;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $task;
    public $user;
    public $emailAddressToNotify;

    /**
     * TaskCreated constructor.
     * @param Task $task
     * @param User $user
     * @param null $emailAddressToNotify
     */
    public function __construct(Task $task, User $user, $emailAddressToNotify = null)
    {
        $this->task = $task;
        $this->user = $user;
        $this->emailAddressToNotify = $emailAddressToNotify;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
