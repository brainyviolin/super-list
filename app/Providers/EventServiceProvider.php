<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\TaskCreated' => [
            'App\Listeners\CreateTaskCreatedLog',
            'App\Listeners\SendTaskCreatedNotificationEmail'

        ],
        'App\Events\TaskCompleted' => [
            'App\Listeners\CreateTaskCompletedLog',
        ],
        'App\Events\TaskDeleted' => [
            'App\Listeners\CreateTaskDeletedLog',
        ],
        'App\Events\TaskUpdated' => [
            'App\Listeners\CreateTaskUpdatedLog'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
