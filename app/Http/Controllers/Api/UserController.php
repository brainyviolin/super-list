<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\User;


class UserController extends ApiBaseController
{

    public function index()
    {
        return $this->response(User::all(), [], 200);
    }

    public function authenticatedUser()
    {
        return $this->response(Auth::user(), [], 200);
    }

}
