<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class ApiBaseController extends Controller
{


    public function response($data, $errors, $code)
    {
        return response()->json([
            'data' => $data,
            'errors' => $errors,
            'code' => $code
        ], 200);
    }

}
