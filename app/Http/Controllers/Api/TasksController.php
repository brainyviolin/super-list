<?php

namespace App\Http\Controllers\Api;

use App\Events\TaskCompleted;
use App\Events\TaskCreated;
use App\Events\TaskDeleted;
use App\Events\TaskUpdated;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class TasksController extends ApiBaseController
{

    public function index()
    {
        return $this->response(Task::with('taskLogs.user')->get(), [], 200);
    }

    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'text' => 'string|required',
            'due_date' => 'date',
            'email_address' => 'email'
        ]);

        if($validation->fails())
        {
            return $this->response([], $validation->errors(), 400);
        }

        $input = $request->only(['text', 'due_date']);

        //@TODO :rename this method
        $task = Task::createNew($input);

        event(new TaskCreated($task, $request->user(), $request->input('email_address')));

        return $this->response($task->toArray(), [], 200);

    }

    public function show($id)
    {
        $task = Task::with('taskLogs.user')->find($id);

        if(!$task)
        {
            return $this->response([],['Task could not be found'], 404);
        }

        return $this->response($task->toArray(), [], 200);
    }

    public function update(Request $request, $id)
    {

        $task = Task::find($id);

        if(!$task)
        {
            return $this->response([], ['Task not found'], 404);
        }

        $validation = Validator::make($request->all(), [
            'text' => 'required|string',
            'due_date' => 'date'
        ]);

        if($validation->fails())
        {
            return $this->response([], $validation->errors(), 400);
        }

        $task->fill(array_filter($request->only(['text', 'due_date'])));

        $task->save();

        event(new TaskUpdated($task, $request->user()));

        return $this->response([], [], 200);


    }

    /**
     * Update the order of a single task
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOrder(Request $request, $id)
    {
        $task = Task::find($id);

        if(!$task)
        {
            return $this->response([], ['Task not found'], 404);
        }

        $validation = Validator::make($request->all(), [
            'order' => 'required|integer',
        ]);

        if($validation->fails())
        {
            return $this->response([], $validation->errors(), 400);
        }

        $task->order = $request->input('order');

        $task->save();

        return $this->response([], [], 200);

    }

    /**
     * Mark a single task as complete
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsCompleted(Request $request, $id)
    {
        $task = Task::find($id);

        if(!$task)
        {
            return $this->response([], ['Task not found'], 404);
        }

        if($task->completed)
        {
            return $this->response([], ['This task is already complete'], 400);
        }

        $task->markAsCompleted();

        event(new TaskCompleted($task, $request->user()));

        return $this->response([], [], 200);

    }

    public function delete(Request $request, $id)
    {
        $task = Task::find($id);

        if(!$task)
        {
            return $this->response([], ['Task not found'], 404);
        }

        $task->delete();

        event(new TaskDeleted($task, $request->user()));

        return $this->response([], [], 200);
    }

}
