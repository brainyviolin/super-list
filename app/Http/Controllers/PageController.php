<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 18/05/17
 * Time: 20:53
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    public function home()
    {
        return view('home');
    }

    public function login()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'email|required',
            'password' => 'string|required'
        ]);

        if($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors());
        }

        if(\Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
            return redirect()->to('/');
        }

        return redirect()->back()->with('message', 'We failed to find your secret identity');

    }

    public function logout(Request $request)
    {
        Auth::logout();

        return redirect()->to('/login');
    }

}