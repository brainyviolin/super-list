<?php

namespace App\Listeners;

use App\Events\TaskCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendTaskCreatedNotificationEmail implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  TaskCreated  $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        if($event->emailAddressToNotify)
        {
            Mail::send('email.newTask', ['user' => $event->user, 'task' => $event->task], function($message) use($event) {

                $message->to($event->emailAddressToNotify);
                $message->from('no-reply@superlist.com');

            });
        }
    }
}
