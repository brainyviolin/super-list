<?php

namespace App\Listeners;

use App\Events\TaskDeleted;
use App\TaskLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTaskDeletedLog
{


    /**
     * Handle the event.
     *
     * @param  TaskDeleted  $event
     * @return void
     */
    public function handle(TaskDeleted $event)
    {
        TaskLog::create([
            'user_id' => $event->user->id,
            'task_id' => $event->task->id,
            'event' => 'deleted'
        ]);
    }
}
