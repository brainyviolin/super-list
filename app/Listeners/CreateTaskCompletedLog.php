<?php

namespace App\Listeners;

use App\Events\TaskCompleted;
use App\Task;
use App\TaskLog;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTaskCompletedLog
{

    /**
     * Handle the event.
     *
     * @param  TaskCompleted $event
     * @return void
     */
    public function handle(TaskCompleted $event)
    {

        TaskLog::create([
            'event' => 'completed',
            'user_id' => $event->user->id,
            'task_id' => $event->task->id
        ]);

    }
}
