<?php

namespace App\Listeners;

use App\Events\TaskCreated;
use App\TaskLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTaskCreatedLog
{

    /**
     * Handle the event.
     *
     * @param  TaskCreated  $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        TaskLog::create([
            'user_id' => $event->user->id,
            'task_id' => $event->task->id,
            'event' => 'created'
        ]);
    }
}
