<?php

namespace App\Listeners;

use App\Events\TaskUpdated;
use App\TaskLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTaskUpdatedLog
{


    /**
     * Handle the event.
     *
     * @param  TaskUpdated  $event
     * @return void
     */
    public function handle(TaskUpdated $event)
    {
       TaskLog::create([
            'user_id' => $event->user->id,
            'task_id' => $event->task->id,
            'event' => 'updated'
        ]);


    }
}
