export const ADD_MESSAGE = 'ADD_MESSAGE';

export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';

let nextNotificationID  = 0;

export function message(message, type)
{
    console.log('sdsdsd');
    return function(dispatch)
    {
        const id = nextNotificationID ++;
        dispatch(addMessage(message, type, id));
        setTimeout(() => {  
            dispatch(removeMessage(id));
        }, 5000);

    }  

}

export function addMessage(message, type, id)
{
    return {
                type: ADD_MESSAGE,
                payload : {
                    type : 'success',
                    text : message,
                    id : id
                }
        }
        
}
    


export function removeMessage(id)
{
    return {
        type : REMOVE_MESSAGE,
        payload : id
    }
}   