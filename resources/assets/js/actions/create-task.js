import connection from '../connection';

import { getTaskHistory } from './tasks';
import { message } from './messages'; 

export const UPDATE_CREATE_TASK_VALUE = 'UPDATE_CREATE_TASK_VALUE';

export const SET_CREATE_TASK_ERRORS = 'SET_CREATE_TASK_ERRORS';

export const ADD_NEW_TASK = 'ADD_NEW_TASK';



export function addNewTask(task)
{
    return {
        type: ADD_NEW_TASK,
        payload : task
    }
}

export function saveNewTask(text, email = null)
{

    return function(dispatch)
    {
        let params = {
            text : text
        }

        if(email)
        {
            params.email_address = email;
        }

        connection({
            method : 'POST',
            url : `/tasks`,
            data: params,
            success: response => {

                if(response.data.code == 200)
                {   
                    if(email)
                    {  
                        
                       dispatch(message(`Invite to save the day was sent to ${email}`, 'success'));
                    }
                    dispatch(addNewTask(response.data.data));
                    dispatch(getTaskHistory(response.data.data.id));
                }
            },
            error : response => {
               
                dispatch(message('Whoops! Something went wrong', 'error'));
            }

        });
    }
}

export function setCreateTaskErrors(name, errors)
{   

    return {
        type : SET_CREATE_TASK_ERRORS,
        payload : {
            name : name,
            errors : errors
        }
    }
}

export function updateCreateTaskValue(name, value)
{
    return {
        type : UPDATE_CREATE_TASK_VALUE,
        payload : {
            name : name,
            value: value
        }
    }
}