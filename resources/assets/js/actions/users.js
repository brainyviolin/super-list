import connection from '../connection';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';


export function setCurrentUser(user)
{
    
    return {
        type : SET_CURRENT_USER,
        payload : user
    }

}

export function getAuthenticatedUser()
{
    return function(dispatch)
    {
        connection({
            method : 'GET',
            url : '/users/me',
            success: response => {
           
                if(response.data.code == 200)
                {
                    dispatch(setCurrentUser(response.data.data))
                }
            },
            error : response => {

            }

        });
    }
}