import connection from '../connection';
import { message } from './messages'; 

export const SET_TASKS = 'SET_TASKS';

export const SET_FETCHING_TASKS = 'SET_FETCHING_TASKS';

export const SET_SAVING_TASK = 'SET_SAVING_TASK';

export const UPDATE_TASK_TEXT = 'UPDATE_TASK_TEXT';

export const REMOVE_TASK = 'REMOVE_TASK';

export const CHANGE_TASK_COMPLETE_STATUS = 'CHANGE_TASK_COMPLETE_STATUS';

export const SET_CURRENT_TASK = 'SET_CURRENT_TASK';

export const SET_TASK_HISTORY  = 'SET_TASK_HISTORY';


export function setCurrentTask(task)
{
    return {
        type : SET_CURRENT_TASK,
        payload : task
    }
}

export function updateTaskText(id, text)
{
    return {
        type : UPDATE_TASK_TEXT,
        payload : {
            id: id,
            text : text
        }
    }
}

export function setSavingTask(status)
{
    return {
        type :SET_SAVING_TASK,
        payload : status
    }
}

export function setFetchingTasks(status)
{
    return {
        type : SET_FETCHING_TASKS,
        payload : status
    }
}

export function setTasks(tasks)
{
    return {
        type : SET_TASKS,
        payload: tasks
    }
}

export function fetchTasks()
{
    return function(dispatch){
        dispatch(setFetchingTasks(true));
        

        connection({
            method : 'GET',
            url : '/tasks',
            success: response => {
                dispatch(setFetchingTasks(false));
                if(response.data.code == 200)
                {
                    dispatch(setTasks(response.data.data))
                }
            },
            error : response => {
                dispatch(message('Whoops! Something went wrong', 'error'));
            }

        });
    }
}

export function setTaskHistory(id, logs)
{

    return {
        type : SET_TASK_HISTORY,
        payload : {
            logs : logs,
            id: id
        }
    }
}

export function getTaskHistory(id)
{
     return function(dispatch){

        connection({
            method : 'GET',
            url : `/tasks/${id}`,
            success: response => {
                if(response.data.code == 200)
                {   
                    dispatch(setTaskHistory(id, response.data.data.task_logs))
                }
            },
            error : error => {
                dispatch(message('Whoops! Something went wrong', 'error'));
            }

        });
    }
} 

export function saveTask(id, value)
{

    return function(dispatch)
    {
        dispatch(setSavingTask(true));
        connection({
            method : 'POST',
            url : `/tasks/${id}`,
            data: {
                text:  value
            },
            success: response => {

                dispatch(setSavingTask(false));

                if(response.data.code == 200)
                {
                    dispatch(getTaskHistory(id));
                }
            },
            error : response => {
                dispatch(setSavingTask(false));
                dispatch(message('Whoops! Something went wrong', 'error'));
            }

        });
    }
     
}


export function removeTask(id)
{
    return {
        type: REMOVE_TASK,
        payload : id
    }
}

export function deleteTask(id)
{
    return function(dispatch) {

          connection({
            method : 'POST',
            url : `/tasks/${id}/delete`,
            success: response => {

                if(response.data.code == 200)
                {
                    dispatch(setCurrentTask(null));

                    dispatch(removeTask(id));
                }
            },
            error : response => {
                dispatch(message('Whoops! Something went wrong', 'error'));
            }

        });
    }

}

export function changeTaskCompleteStatus(id, value)
{
    return {
        type : CHANGE_TASK_COMPLETE_STATUS,
        payload : {
            id : id,
            value : value
        }
    }
}

export function markTaskComplete(id)
{
    return function(dispatch)
    {

         connection({
            method : 'POST',
            url : `/tasks/${id}/mark-completed`,
            success: response => {

                if(response.data.code == 200)
                {
                    dispatch(setSavingTask(false));
                    dispatch(changeTaskCompleteStatus(id, 1));
                    dispatch(getTaskHistory(id));
                }

            },
            error : response => {
                dispatch(message('Whoops! Something went wrong', 'error'));
         
         
            }

        });
    }
}