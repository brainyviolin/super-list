import { 
    SET_TASKS, 
    SET_FETCHING_TASKS, 
    SET_SAVING_TASK, 
    UPDATE_TASK_TEXT, 
    REMOVE_TASK, 
    CHANGE_TASK_COMPLETE_STATUS, 
    ADD_NEW_TASK,
    SET_CURRENT_TASK,
    SET_TASK_HISTORY
} from '../actions';

import { updateArrayById, removeTaskById } from '../helpers/helpers';

const initialState = {
    tasks : [],
    currentTask : null,
    fetching : false,
    saving : false
}

export default function tasks(state = initialState, action)
{
    switch(action.type)
    {   
        case SET_TASK_HISTORY :
            return Object.assign({}, state, {
                tasks : updateArrayById(state.tasks, action.payload.id, 'task_logs', action.payload.logs)
            });
        case SET_CURRENT_TASK :
            return Object.assign({}, state, {
                currentTask : action.payload
            });
        case ADD_NEW_TASK :
            return Object.assign({}, state, {
                tasks : state.tasks.concat(action.payload)
            });
        case CHANGE_TASK_COMPLETE_STATUS :
            return Object.assign({}, state, {
                tasks : updateArrayById(state.tasks, action.payload.id, 'completed', action.payload.value)
            });

        case REMOVE_TASK : 
            return Object.assign({}, state, {
                tasks : removeTaskById(state.tasks, action.payload)
            });
        case UPDATE_TASK_TEXT :
            return Object.assign({}, state, {
                tasks : updateArrayById(state.tasks, action.payload.id, 'text', action.payload.text)
            });

        case SET_SAVING_TASK :
            return Object.assign({}, state, {
                saving : action.payload
            });
        case SET_FETCHING_TASKS :
            return Object.assign({}, state, {
                fetching : action.payload
            });
        case SET_TASKS :
            return Object.assign({}, state, {
                tasks: action.payload
            });
        default :
            return state;
    }
}