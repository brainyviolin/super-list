import { UPDATE_CREATE_TASK_VALUE, SET_CREATE_TASK_ERRORS, ADD_NEW_TASK } from '../actions';
import { updateArrayById, removeTaskById } from '../helpers/helpers';

const initialState = {
    text : '',
    email : '',
    errors : {
        email : [],
        text  : []
    },
    saving : false
}

export default function tasks(state = initialState, action)
{
    switch(action.type)
    {
        case ADD_NEW_TASK : 
            return Object.assign({}, state, {
                text : '',
                email : ''
            });
        case SET_CREATE_TASK_ERRORS : 
            return Object.assign({}, state, {
                errors : Object.assign({}, state.errors, {
                    [action.payload.name] : action.payload.errors
                })
            });
        case UPDATE_CREATE_TASK_VALUE :
            return Object.assign({}, state, {
                [action.payload.name] : action.payload.value
            });
        default :
            return state;
    }
}