import { SET_CURRENT_USER } from '../actions';

const initialState = {

    authenticatedUser : {
        name : 'Name',
        avatar_url : '/images/default_user.png'
    },
    users : []

}

export default function users(state = initialState, action)
{   

    switch(action.type)
    {   
        case SET_CURRENT_USER : 
            return Object.assign({}, state, {
                authenticatedUser : action.payload
            });
        default :
        return state;
    }

}