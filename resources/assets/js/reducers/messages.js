import { ADD_MESSAGE, REMOVE_MESSAGE } from '../actions';
import { removeTaskById } from '../helpers/helpers';

const initialState = {

    messages : []

}

export default function users(state = initialState, action)
{   

    switch(action.type)
    {   
        case ADD_MESSAGE : 
            return Object.assign({}, state, {
                messages : state.messages.concat(action.payload)
            });
        case REMOVE_MESSAGE : 
            return Object.assign({}, state, {
                messages : removeTaskById(state.messages, action.payload)
            });
        default :
        return state;
    }

}