import { combineReducers } from 'redux';
import tasks from './tasks';
import createTask from './create-task';
import users from './users';
import messages from './messages';


const rootReducer = combineReducers({
    tasks : tasks,
    createTask : createTask,
    users : users,
    messages : messages
});

export default (state, action) => {
    return rootReducer(state, action);
}