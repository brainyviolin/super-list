import React from 'react';
import { connect } from 'react-redux';


class MainLayout extends React.Component {

    render(){
        return(
            <div id="main">
                <div className="contentContainer">
                    { this.props.children }
                </div>  
            </div>
           
        );
    }
}

function mapStateToProps(state)
{
    return {
        state : state
    }
}

export default connect(mapStateToProps)(MainLayout);
