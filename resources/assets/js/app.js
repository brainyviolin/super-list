require('./bootstrap');

import ReactDom from 'react-dom';
import React from 'react';
import MainLayout from './layouts/main';
import { Provider } from 'react-redux';
import Store from './store'
import Tasks from './containers/tasks';



ReactDom.render(
    <Provider store={Store} >
        <MainLayout >
            <Tasks />                
        </MainLayout>
    </Provider >
    ,
    document.getElementById('App')
    );