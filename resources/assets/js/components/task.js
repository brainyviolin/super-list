import React from 'react';
import { ucFirst } from '../helpers/helpers';

export default class Task extends React.Component {


    handleTextUpdate(e)
    {
        this.props.onTextUpdate(this.props.item.id, e.target.value);
    }

    handleSave(e)
    {
        this.props.onSave(this.props.item.id, e.target.value);
    }

    handleComplete()
    {
        this.props.onComplete(this.props.item.id);
    }

    handleDelete()
    {
        this.props.onDelete(this.props.item.id)
    }

    currentClasses()
    {
        let classes = 'task';

        if(this.props.item.completed == 1)
        {
            classes += ' completed';
        }

        return classes;
    }

    isDisabled()
    {
        if(Boolean(this.props.item.completed))
        {
            return true;
        }

        return false;
    }



    handleTaskClick(e)
    {
        if(e.target.className !== 'fa fa-trash-o')
        {
            this.props.onClick(this.props.item);
        }
    }

    render(){

        let latestLog = null;

        if(this.props.item.task_logs && this.props.item.task_logs.length >= 1)
        {
            latestLog = this.props.item.task_logs[(this.props.item.task_logs.length - 1)];
        }

        return(
            <div className={this.currentClasses()} onClick={this.handleTaskClick.bind(this)}>
                <div className="inputWrapper">
                    <input disabled={this.isDisabled()}  type="text" onBlur={this.handleSave.bind(this)} value={this.props.item.text} onChange={this.handleTextUpdate.bind(this)} />
                </div>
                <div className="actions">
                     <div className="doneIcon">
                    { !Boolean(this.props.item.completed) && <i onClick={this.handleComplete.bind(this)} className="fa fa-check" aria-hidden="true"></i> } 
                    </div>
                    <div className="trashIcon">
                        <i onClick={this.handleDelete.bind(this)} className="fa fa-trash-o" aria-hidden="true"></i>
                    </div>
                </div>
                {latestLog && 
                    <div className="recentHistory">
                         <img src={latestLog.user.avatar_url} />
                        {ucFirst(latestLog.event)} by {latestLog.user.name} at {latestLog.created_at}
                    </div>
                }
    
            </div>
        );
    }

}