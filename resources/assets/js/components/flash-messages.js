import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class FlashMessages extends React.Component {

    renderMessages()
    {
        return this.props.messages.messages.map((item, index)=> {

            return(<div key={index} className={`message ${item.type}`}>{item.text}</div>);

        });
    }

    render()
    {
        return(
            <div className="flashMessages">
                {this.renderMessages()}
            </div>
        );
    }

}

function mapStateToProps(state)
{
    return {
        messages : state.messages
    }
}

export default connect(mapStateToProps, actions)(FlashMessages);