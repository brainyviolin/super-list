import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';


class Header extends React.Component {

    signOut()
    {
        window.location = '/logout';
    }


    render(){
        return(
            <div id="Header">
                <div className="user">
                    <img src={this.props.users.authenticatedUser.avatar_url} />
                    {this.props.users.authenticatedUser.name}
                </div>
                <div className="menu">
                        <button onClick={this.signOut.bind(this)} >Sign Out</button>
                </div>
            </div>
        );
    }

}


function mapSateToProps(state)
{
    return {

        users : state.users
    }
}

export default connect(mapSateToProps, actions)(Header);