import React from 'react';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc'; 


const SortableItem = SortableElement(({value})=>{
    return(<div className="sortableContentItem">{value}</div>);
});


const SortableList = SortableContainer(({items}) => {
  return (
    <div className="sortableContentList">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value}  />
      ))}
    </div>
  );
});


export default class SortableContentList extends React.Component {

    componentWillMount()
    {
        this.state = {
            items : this.sortContent(this.props.contentList)
        }

    }
    componentWillReceiveProps(nextProps)
    {
    

        this.sortContent(nextProps.contentList);
        this.setState({
            items : this.sortContentComponents(nextProps.contentList)
        })
    }

    sortContentComponents(components)
    {   
    
        return components.sort((a,b)=>{
            return a.props.content.order - b.props.content.order;
        });

    }

    sortContent(content)
    {   
    
        return content.sort((a,b)=>{
            return a.order - b.order;
        });

    }

    updateOrder(newOrder)
    {
        newOrder.forEach((item) => {

        });

        let updated = newOrder.map((item, index)=>{
            let content = item.props.content;
            content.order = (index + 1);
            return content;
        });

       

        this.props.onUpdate(updated);
    }   

    onSortEnd({oldIndex, newIndex})
    {
        let newOrder = arrayMove(this.state.items, oldIndex, newIndex);

       
         this.setState({
            items: arrayMove(this.state.items, oldIndex, newIndex),
        });


       this.updateOrder(newOrder);
    }

    render() {
        return <SortableList axis="y" lockAxis="y"	 useDragHandle={true} items={this.state.items} onSortEnd={this.onSortEnd.bind(this)} />;
     }
}