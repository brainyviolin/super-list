import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import { ucFirst } from '../helpers/helpers';

class TaskInfo extends React.Component {

    getCurrentTask()
    {
        if(this.props.tasks.currentTask)
        {
            for(let i =0; i < this.props.tasks.tasks.length; i++)
            {
                if(this.props.tasks.tasks[i].id == this.props.tasks.currentTask.id)
                {   
                    return this.props.tasks.tasks[i];
                }
            }
        }

        return null;
        
    }

    renderHistory()
    {
        return this.getCurrentTask().task_logs.map((item, index) => {

            return(
                    <li key={index}>
                        <img src={item.user.avatar_url} />
                       {ucFirst(item.event)} by {item.user.name} at {item.created_at}
                    </li>
                );

        });
    }

    render()
    {   
        let task = {};
        if(this.props.tasks.currentTask)
        {
            task = this.getCurrentTask();
        }

        return(
            <div className="taskInfo" >
                {
                    this.props.tasks.currentTask && 
                    <div className="wrapper">
                        <div className="text" >{task.text}</div>
                        <div className="history">
                            <ul>
                                {this.renderHistory()}
                            </ul>
                        </div>
                    </div>
                }
            </div>
        );
    }

}

function mapStateToProps(state)
{
    return {
        tasks : state.tasks
    }
}

export default connect(mapStateToProps, actions)(TaskInfo);