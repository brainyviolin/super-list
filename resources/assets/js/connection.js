import axios from 'axios';

const client = axios.create({
    'baseURL' : '/api'
});

const defaultSucess = (response) => {

    console.log(response);

}

const defaultError = (error) => {

        console.log(error);
    
}


export default function connection(options)
{

    return client(options).then(options.success).catch(error => {
        


        if(error.response.status == 401)
        {
            window.location = '/logout';
            return;
        }

        options.error(error);
  
    });


}