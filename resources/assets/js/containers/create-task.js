import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class CreateTasks extends React.Component {


 
    validation()
    {   
        let errorCount = 0;
        if(this.props.createTask.text.length < 1)
        {
            this.props.setCreateTaskErrors('text', ['This required some text']);
            errorCount ++;
        }

        if(this.props.createTask.email.length >= 1)
        {
            this.props.setCreateTaskErrors('email', ['This needs to be a vaild email']);
            errorCount ++;

        }

        return errorCount;

    }

    renderErrors(type)
    {
        return(
            <ul>
                { this.props.createTask.errors[type].map((item, index)=>{
                    return <li key={index}>{item}</li>
                }) }
            </ul>
        );
    }

    hasErrors(type)
    {
        if(this.props.createTask.errors[type].length >= 1)
        {
            return true;
        }

        return false;
    }


    handleChange(e)
    {   
        this.props.setCreateTaskErrors(e.target.name, []);
        this.props.updateCreateTaskValue(e.target.name, e.target.value);
    }

    handleSave()
    {
        if(this.validation() === 0)
        {
            this.props.saveNewTask(this.props.createTask.text);
        }
    }

    render()
    {
        return (
            <div className="createTask">
                <div className="text">
                    <input name="text" value={this.props.createTask.text} onChange={this.handleChange.bind(this)} type="text" placeholder="Text" />
                    <div className="errors">{this.hasErrors('text') &&  this.renderErrors('text') }</div>
                </div>
                <div className="email">
                    <input name="email" type="text" value={this.props.createTask.email} onChange={this.handleChange.bind(this)}  placeholder="Email address to notify" />
                    <div className="errors">{this.hasErrors('email') &&  this.renderErrors('email') }</div>

                </div>
                <div className="actions">
                    <div className="spinner"></div>
                    <div className="add">
                        <button onClick={this.handleSave.bind(this)}>Add</button>
                    </div>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state)
{
    return {
        tasks : state.tasks,
        createTask : state.createTask 
    }
}

export default connect(mapStateToProps, actions)(CreateTasks);