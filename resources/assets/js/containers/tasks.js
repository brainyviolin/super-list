import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Task from '../components/task';

import CreateTask from '../components/create-task';

import Header from '../components/header';

import TaskInfo from '../components/task-info';

import FlashMessages from '../components/flash-messages';

class Tasks extends React.Component {

    componentWillMount()
    {
        this.props.fetchTasks();
        this.props.getAuthenticatedUser();
    }

    handleSaveItem(id, value)
    {
        this.props.saveTask(id, value);
    }

    handleCompleteItem(id)
    {
        this.props.markTaskComplete(id);
    }

    handleDeleteItem(id)
    {
        this.props.deleteTask(id);
    }

    handleClick(item)
    {
        this.props.setCurrentTask(item);
    }

    renderTasks()
    {
        return this.props.tasks.tasks.map(item => {
            return <li key={item.id}><Task onClick={this.handleClick.bind(this)} item={item} onDelete={this.handleDeleteItem.bind(this)} onComplete={this.handleCompleteItem.bind(this)} onTextUpdate={this.props.updateTaskText} onSave={this.handleSaveItem.bind(this)} /></li>
        });
    }

    render(){
        return(
            <div id="TasksMain">
                <div className="tasks">
                    <FlashMessages />
                    <ul>
                        {this.renderTasks()}
                    </ul>
                      <CreateTask />
                </div>
              
                <div className="infoWindow">
                    <Header />
                    <TaskInfo />
                </div>
            </div>
        );
    }

}

function mapStateToProps(state)
{
    return {
        tasks : state.tasks
    }
}

export default connect(mapStateToProps, actions)(Tasks);