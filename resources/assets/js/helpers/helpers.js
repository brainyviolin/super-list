export function updateArrayById(items, id, property, value)
{
    return items.map(item => {
        
                    if(item.id == id)
                    {

                        return Object.assign({}, item, {
                            [property] : value
                        })
                    }

                    return item;
            })
}

export function removeTaskById(items, id)
{
    let key = getIndexFromId(items, id);


    if(key !== null)
    {
        items.splice(key, 1);
    }



    return items;

}

export function getIndexFromId(items, id)
{
    for(let i = 0; i < items.length; i++)
    {
        if(items[i].id == id)
        {
            return i;
        }
    }

    return null;
}

export function ucFirst(text)
{
    return text.charAt(0).toUpperCase() + text.slice(1);
}