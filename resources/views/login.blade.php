@extends('master', ['pageClass' => 'loginPage'])

@section('content')

<div class="outerWrapper">
    <div class="innerWrapper">
        <div class="content">
            <div class="logoText">
                SUPER LIST
            </div>
            <div class="form" >
                <form action="/login" method="POST">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <div class="inputWrapper">
                        <input type="email" name="email" value="{{old('email')}}" placeholder="Email">
                        <div class="errors">
                            @if($errors->get('email'))
                                <ul>
                                    @foreach($errors->get('email') as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="inputWrapper">
                        <input type="password" name="password" value="" placeholder="Password">
                        <div class="errors">
                            @if($errors->get('password'))
                                <ul>
                                    @foreach($errors->get('password') as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="inputWrapper">
                    <button type="submit">Login</button>
                    </div>
                    <div class="message">
                        @if(\Session::has('message'))
                            <span>{{\Session::get('message')}}</span>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop