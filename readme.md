# SUPER LIST
#### A React and Laravel Task Manager for Tutora

### Features
* React & Redux Driven interface
* Authentication using Laravel's Passport
* Add, Complete and delete tasks via an API
* Test Driven (Mostly)
* Tasks history
* Mutiple users
* Notify an email address when a task is added

### Features to implement
* Realtime user collaboration 
* Ordering of tasks
* Completion dates

## Installation 

From the terminal clone the repo:  
```
git clone https://brainyviolin@bitbucket.org/brainyviolin/super-list.git
```

In the project root install dependencies by running (you must have composer installed globablly):
```
composer install 
```
The project uses Laravel's Passport package to handle API authentication. You must create some new keys by running :
```
php artisan passport:keys
```
Create a new `.env` file and copy and paste the contents of the `.env.example` file into your new `.env`. Run the following commangd to create a new app key:
```
php artisan key:generate
```
Change to the Database details to match those of your local enviroment

The project uses queues to send emails so you need to change the `QUEUE_DRIVER` variable to the driver of your choice. The dependiences for `redis` and `beanstalkd` have already been included.

Next the project requires someway of sending emails out. For testing purposes you can set mail enviomental variables to use `mailtrap.io` You need to set up an account and penter your username and password for your inbox to `MAIL_USERNAME` and `MAIL_PASSWORD`

Now the project should be setup to go. You only need to migrate and seed the database.
```
php artisan migrate
php artisan db:seed
```

You can now visit the site in and login with either of the users that have been seeded. Both passwords have been set to `123456`
```
email: superman@fortreeofsolitude.com
email: batman@batcave.com 
```
## Testing 

You can run test from the project root by running:
```
./bin/phpunit
```






