<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class UsersTest extends TestCase
{
    use DatabaseMigrations;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class, 10)->create()[0];
    }
    /**
     * @test
     */
    public function test_it_should_return_200_status()
    {

        $response = $this->actingAs($this->user, 'api')->get('/api/users');

        $response->assertStatus(200);
    }

    public function test_it_should_return_users()
    {
        $response = $this->actingAs($this->user, 'api')->get('/api/users');

        $this->assertCount(10, $response->json()['data']);

    }

    public function test_it_should_return_the_authenticated_user()
    {
        $response = $this->actingAs($this->user, 'api')->get('/api/users/me');

        $this->assertEquals($response->json()['data']['id'], $this->user->id);

    }
}
