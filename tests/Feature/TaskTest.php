<?php

namespace Tests\Feature;

use App\Task;
use App\TaskLog;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Events\TaskCompleted;
use App\Events\TaskDeleted;
use App\Events\TaskUpdated;
use App\Events\TaskCreated;

class TaskTest extends TestCase
{

    use DatabaseMigrations;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     */
    public function test_it_should_return_200_status()
    {

        $response = $this->actingAs($this->user, 'api')->get('/api/tasks');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_is_should_return_all_tasks()
    {
        factory(Task::class, 10)->create();

        $response = $this->actingAs($this->user, 'api')->get('/api/tasks');

        $this->assertCount(10, $response->json()['data']);
    }

    public function test_it_should_return_all_tasks_with_logs()
    {
        factory(TaskLog::class, 1)->create();

        $response = $this->actingAs($this->user, 'api')->get('/api/tasks');

        $this->assertCount(1, $response->json()['data'][0]['task_logs']);
    }

    public function test_it_should_return_all_tasks_with_logs_and_its_user()
    {
        factory(TaskLog::class, 1)->create();

        $response = $this->actingAs($this->user, 'api')->get('/api/tasks');

        $this->assertTrue(isset($response->json()['data'][0]['task_logs'][0]['user']));
    }

    public function test_it_should_return_a_task_with_requested_id()
    {
        factory(Task::class, 10)->create();

        $response = $this->actingAs($this->user, 'api')->get('/api/tasks/1');

        $this->assertEquals(1, $response->json()['data']['id']);

    }

    public function test_it_should_create_a_task()
    {
        $createdText = 'This task was created';

        $task = factory(Task::class)->make(['text' => $createdText]);

        $this->actingAs($this->user, 'api')->post('/api/tasks', $task->toArray());

        $this->assertDatabaseHas('tasks', ['text' => $createdText]);
    }

    public function test_it_should_create_a_task_with_maximum_order()
    {
        factory(Task::class)->create(['order' => 1]);

        $task = factory(Task::class)->make([]);

        $this->actingAs($this->user, 'api')->post('/api/tasks', $task->toArray());

        $this->assertDatabaseHas('tasks', ['order' => 1]);
    }

    public function test_it_should_update_order()
    {
        factory(Task::class)->create(['order' => 1]);

        $newOrder = 50;

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/order', ['order' => $newOrder]);

        $this->assertDatabaseHas('tasks', ['id' => 1, 'order' => $newOrder]);


    }

    public function test_it_should_create_a_task_created_log()
    {
        $createdText = 'This task was created';

        $task = factory(Task::class)->make(['text' => $createdText]);

        $this->actingAs($this->user, 'api')->post('/api/tasks', $task->toArray());

        $this->assertDatabaseHas('task_logs', ['task_id' => 1, 'user_id' => $this->user->id, 'event' => 'created']);

    }

    public function test_it_should_fire_task_created_event()
    {
        $this->expectsEvents(TaskCreated::class);

        $createdText = 'This task was created';

        $task = factory(Task::class)->make(['text' => $createdText]);

        $this->actingAs($this->user, 'api')->post('/api/tasks', $task->toArray());
    }

    /**
     * @test
     */
    public function test_it_should_set_task_as_completed()
    {
        factory(Task::class, 1)->create([
            'completed' => 0
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/mark-completed');

        $this->assertDatabaseHas('tasks', ['id' => 1, 'completed' => 1]);

    }

    public function test_it_should_update_task_text()
    {
        factory(Task::class, 1)->create([
            'text' => 'original',
        ]);

        $updatedText = 'updated';

        $this->actingAs($this->user, 'api')->post('/api/tasks/1', [
            'text' => $updatedText
        ]);

        $this->assertDatabaseHas('tasks', ['id' => 1, 'text' => $updatedText]);

    }

    public function test_it_should_fire_task_updated_event()
    {
        $this->expectsEvents(TaskUpdated::class);

        factory(Task::class, 1)->create([
            'text' => 'original'
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1', [
            'text' => 'updated'
        ]);
    }

    public function test_it_should_create_task_updated_log()
    {

        factory(Task::class, 1)->create([
            'text' => 'original'
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1', [
            'text' => 'updated'
        ]);

        $this->assertDatabaseHas('task_logs', ['task_id' => 1, 'user_id' => $this->user->id, 'event' => 'updated']);

    }


    public function test_it_should_fire_task_completed_event()
    {
        $this->expectsEvents(TaskCompleted::class);

        factory(Task::class, 1)->create([
            'completed' => 0
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/mark-completed');

    }

    public function test_it_should_create_task_completed_log()
    {
        factory(Task::class, 1)->create([
            'completed' => 0
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/mark-completed');

        $this->assertDatabaseHas('task_logs', ['task_id' => 1, 'user_id' => $this->user->id, 'event' => 'completed']);

    }

    public function test_it_should_soft_delete_task()
    {
        factory(Task::class, 1)->create([
            'completed' => 0
        ]);
        $this->actingAs($this->user, 'api')->post('/api/tasks/1/delete');

        $this->assertSoftDeleted('tasks', ['id' => 1]);
    }

    public function test_it_should_fire_task_deleted_event()
    {
        $this->expectsEvents(TaskDeleted::class);

        factory(Task::class, 1)->create([
            'completed' => 0
        ]);

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/delete');

    }

    public function test_it_should_create_task_deleted_log()
    {
        factory(Task::class, 1)->create();

        $this->actingAs($this->user, 'api')->post('/api/tasks/1/delete');

        $this->assertDatabaseHas('task_logs', ['task_id' => 1, 'user_id' => $this->user->id, 'event' => 'deleted']);

    }

}
