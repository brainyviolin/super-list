<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Batman',
            'email' => 'batman@batcave.com',
            'password' => \Hash::make('123456'),
            'avatar_url' => '/images/batman.jpg'
        ]);
        App\User::create([
            'name' => 'Superman',
            'email' => 'superman@fortreeofsolitude.com',
            'password' => \Hash::make('123456'),
            'avatar_url' => '/images/superman.jpg'

        ]);
    }
}
