<?php

use Illuminate\Database\Seeder;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\TaskLog::class, 10)->create(['user_id' => rand(1, 2)]);
    }
}
