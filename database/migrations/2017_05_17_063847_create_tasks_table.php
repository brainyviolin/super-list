<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('text')->nullable();
            $table->integer('order')->default(0);
            $table->dateTime('due_date')->nullable();
            $table->tinyInteger('completed')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('tasks');
    }
}
